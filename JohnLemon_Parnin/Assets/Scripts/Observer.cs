﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;


public class Observer : MonoBehaviour
{
    public Transform player; // dectect player
    bool m_IsPlayerInRange;
    public GameEnding gameEnding;
    void OnTriggerEnter (Collider other)
    {
        if(other.transform == player) // is player in range
        {
            m_IsPlayerInRange = true;
        }
    }
    void OnTriggerExit(Collider other) // did player leave trigger area
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }
    void Update ()
    {
        if(m_IsPlayerInRange)
        {
            Vector3 direction = player.position - transform.position + Vector3.up; // creates vector3 to create dirctoin towards player
            Ray ray = new Ray(transform.position, direction); // created a ray
            RaycastHit raycastHit;// define Racast hit varible
            if (Physics.Raycast(ray, out raycastHit)) // if anything is in range
            {
                if (raycastHit.collider.transform == player) // see what has been hit
                {
                    gameEnding.CaughtPlayer();
                }
            }
        }
        
    }
}
