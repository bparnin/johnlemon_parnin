﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseGlow : MonoBehaviour
{
    public GameObject MouseLight; 
    // Start is called before the first frame update
    void OnMouseOver()
    {
        //If your mouse hovers over the GameObject with the script attached, output this message
       
        MouseLight.SetActive(true);
    }

    void OnMouseExit()
    {
        //The mouse is no longer hovering over the GameObject so output this message each frame
       
        MouseLight.SetActive(false);
    }
}
