﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;


public class PlayerMovement : MonoBehaviour
{
    public TextMeshProUGUI HealthText; // Health text
    public int health =3; // starting health
    public float turnSpeed = 20f; // how fast the player turns
    public GameEnding gameEnding;
    public GameObject myLazer;
    public GameObject myLazerSec;
    public GameObject myLazerTH;
    public GameObject m_Glasses;
    public GameObject m_DoorGhostOpen;
    public GameObject m_DoorGhost;
    public GameObject m_PartyHat;
    public GameObject m_NoPartyHat;
    public GameObject m_Door; 



    Animator m_Animator;
    Rigidbody m_Rigidbody;
    Vector3 m_Movement; // create a vector3 varable called m_Movement
    Quaternion m_Rotation = Quaternion.identity; // stores rotations
    AudioSource m_AudioSource; //set audio varrable

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator> (); // set m_Animator variable equal to animator componet
        m_Rigidbody = GetComponent<Rigidbody>(); // set m_Rigidbody to player Rigidbody Componet
        m_AudioSource = GetComponent<AudioSource>(); //set m_AudioSource to player audio compnenet
        //m_Glasses = GetComponent<Glasses>(); //Get Sunglass componet

    SetHealthText(); // set health to text
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal"); // Create a new float varible = to Horzontal axis

        float vertical = Input.GetAxis("Vertical"); // Create a new float varible = to Horzontal axis

        m_Movement.Set(horizontal, 0f, vertical); // set vaule for the x and z for the vector and floated the y position

        m_Movement.Normalize(); // Normalize vector to ensure vector has same magnitude

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f); // takes two float and returns a bool to see if are appromitaly true or false

        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);// same as bool has Horizonal Input except for Vertical

        bool isWalking = hasHorizontalInput || hasVerticalInput; // 
        if (isWalking) // if walk play audio
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop(); //if not walk then stop audio
        }

        m_Animator.SetBool("IsWalking", isWalking); //Set parameter of Animator compnents
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f); // 
        m_Rotation = Quaternion.LookRotation(desiredForward); // creates rotation within the parameters
    }


    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition (m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude); // taking the position of the rigdbody and moving it base on animation and input 
        m_Rigidbody.MoveRotation(m_Rotation); // rotation is added to rigidbody
    }

    void SetHealthText()
    {
        HealthText.text = "health: " + health.ToString(); // add   health to canvas

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("GotKey")) //if player has key
        {
            Destroy(myLazer); //destory lazer
            other.gameObject.SetActive(false); // destroy key
        }
        if (other.gameObject.CompareTag("GuitarTag")) //if player has Guitar
        {
            
            other.gameObject.SetActive(false); // disable iteam
            Destroy(myLazerSec);
            Destroy(myLazerTH);

        }
        if (other.gameObject.CompareTag("SunglassesTag")) //if player has Guitar
        {
            m_Glasses.SetActive(true); // disable iteam
            other.gameObject.SetActive(false); // disable iteam
            m_DoorGhostOpen.SetActive(true); // enable new door ghost
            m_DoorGhost.SetActive(false); // disable old ghost

        }
        if (other.gameObject.CompareTag("PartyHatTag")) //if player has Guitar
        {
            
            other.gameObject.SetActive(false); // disable iteam
            m_PartyHat.SetActive(true); // enable cat with hat
            m_NoPartyHat.SetActive(false); // disable cat with no hat
            m_Door.SetActive(false); // disable door

        }


        
    }
        private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Lazer")) // hit a lazer
        {

            health--;            //health goes down by one
            SetHealthText();
            if (health <= 0)       //if health is 0 restart scene
            {
                gameEnding.CaughtPlayer(); //game restarts
            }
        }
    }
    


}