﻿using System.Collections;
using System.Collections.Generic;
using System.Media;
using System.Runtime.Hosting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public GameObject player;
    bool m_IsPlayerAtExit; // is player escaped
    bool m_IsPlayerCaught; // is player caught
    float m_Timer;
    bool m_HasAudioPlayed;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public CanvasGroup caughtBackgroundImageCanvasGroup; // allow canvas group to display
    public AudioSource exitAudio; // adds exit audio
    public AudioSource caughtAudio; // adds caught audio
    public float displayImageDuration = 1f;
       
    
    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject == player)
            { 
            m_IsPlayerAtExit = true;
            }
    }
    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }
    void Update()
    {
        
        if(m_IsPlayerAtExit) // if player at exit end game
            {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio); // display escape endlevel
            }
        else if(m_IsPlayerCaught) // and if caught display caught endlevel
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
            
        }
    }

  

       public void EndLevel (CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)// if audio has played
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;
        if (m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0); //reload scene
            }
            else
            {
                Application.Quit();// end game
            }
        }
    }
    
}

