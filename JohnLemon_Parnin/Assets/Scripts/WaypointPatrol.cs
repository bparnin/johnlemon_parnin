﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.AI;// enable Unity AI/NavMeshAgent

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent; //enable to added navmeshAgent as a refrence in the INspector Window
    public Transform[] waypoints; // public array
    int m_CurrentWaypointIndex; // store current index of the waypoint array
    
    
    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent.SetDestination(waypoints[0].position); //sets inital destination of NavMeshAgent
    }

    // Update is called once per frame
    void Update()
    {
        if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance) // see if NavMeshAgent has reached its desitioantion
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length; // add one to index
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);// update destination of NavMeshAgent
        }
    }
}
