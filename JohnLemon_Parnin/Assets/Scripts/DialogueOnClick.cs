﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class DialogueOnClick : MonoBehaviour

{
    public GameObject player; // refrence to player 
    public GameObject dialoguePanel; // reference to dialougepanel
    public string dialogueToDisplay; // 
    public Text dialogueText; //
    bool dialogueOpened; // dialogue open yes or no
    bool canClose = true;
   


    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))// if mouse is down 
        {


            if (dialogueOpened) // if dialogue is open
            {
                if (!canClose) // allows to close panel
                {
                    canClose = true;
                }
                else
                {
                    ClosedDialoguePanel();
                }
            }


        }

    }

    private void OnMouseDown() // if mouse is down open dialogue panel
    {
        if (!dialogueOpened)
            OpenDialoguePanel();



    }

    void OpenDialoguePanel()
    {
        dialogueOpened = true;
        Time.timeScale = 0f; // pause game

        dialoguePanel.SetActive(true); // open dialogue panel
        dialogueText.text = dialogueToDisplay;
        canClose = false; 
    }
    void ClosedDialoguePanel()
    {
        dialogueOpened = false;
        Time.timeScale = 1f; // resume game 
        dialoguePanel.SetActive(false); // close dialogue panel



    }


}

